### About Genoviz SDK

The GenoViz SDK is a Java framework for building genome visualization tools. 

See: 

[Genoviz Software Development Kit: Java tool kit for building genomics visualization applications.](https://www.ncbi.nlm.nih.gov/pubmed/19706180)

Applications that use GenoViz SDK:

* Integrated Genome Browser 
* ProtAnnot (an IGB pluggable App)

### Maven repository

https://nexus.bioviz.org/repository/maven-releases/


### License 

This source code is released under the Common Public License, v1.0, 
an OSI approved open source license.

See http://genoviz.sourceforge.net


### GenoViz SDK Tutorial

This contains multiple web pages which gives an overview of the widgets offered by GenoViz SDK. These web pages use JApplet to display the information related to GenoViz SDK Widgets.
The information about GenoViz SDK Widgets is in the form of a code snippet and its explanation, explanation on how a widget works and try it yourself exercises to get a deeper insight of the GenoViz SDK library.


